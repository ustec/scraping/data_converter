#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from abc import ABC, abstractmethod
import shutil
from sqlalchemy import create_engine, MetaData, Table, Column, String, ForeignKey
from sqlalchemy.orm import sessionmaker
import tempfile
from sqlalchemy import exc
from sqlalchemy import event
from sqlalchemy.pool import Pool
import os
from sh import pdftotext
from sqlalchemy.exc import IntegrityError
import re

class DataConverter(ABC):
    """ Extracts data from source and converts to another format """

    def __init__(self):
        # List with objects containing desired fields
        self.objects = []
        self.eng = create_engine(self.config.sqlalchemy_engine_string())
        self.metadata = MetaData(self.eng)
        self.table = Table(self.config.table, self.metadata, autoload=True)
        Session = sessionmaker(bind=self.eng)
        self.session = Session()
    
    @abstractmethod
    def get_input_files(self):
        """Gets the data from its location and stores it"""
        pass
    
    def remove_data(self):
        shutil.rmtree(self.config.raw_data_dir)
        os.mkdir(self.config.raw_data_dir)
        if hasattr(self.config, 'data_dir'):
            shutil.rmtree(self.config.data_dir)
            os.mkdir(self.config.data_dir)

    def get_data(self, remove_files=True):
        if remove_files:
            self.remove_data()
        self.get_input_files()

    @abstractmethod
    def convert(self):
        """Converts initial source of data to a more readable format"""
        if hasattr(self.config, 'data_dir'):
            shutil.rmtree(self.config.data_dir)
            os.mkdir(self.config.data_dir)

    def parse(self):
        """Extracts desired fields from data"""
        # Get records
        raw_records = self.get_valid_records()
        self.build_objects(raw_records)
        
    @abstractmethod
    def get_valid_records(self):
        """Get valid records with all data"""
        pass
    
    @abstractmethod
    def build_objects(self):
        """Get data from valid records and build final objects with desired data"""
        pass

    def clean_array(self, array, contains_list=[]):
        """
        Create a cleaned array with the same rows as the original one,
        except those that contain any string present in contains_list.
        """
        clean_array = []
        for row in array:
            append_row = True
            for cell in row: 
                for s in contains_list:                    
                    if cell and s in cell:
                        append_row = False
                        break
                if not append_row:
                    break
            if append_row:
                clean_array.append(row)
        return clean_array
                                        
        
    
    def clean_source(self, f, contains_list=[], blank_lines=True, exact_list=[]):
        """
        Remove lines that contains all strings in contains and blank lines
        if blank_lines is True. Original source will be replaced with cleaned
        one.
        """
        with open(f, "r") as r, \
                tempfile.NamedTemporaryFile(mode='w',
                                            delete=False) as w:
            # Loop all lines of source
            for line in r:
                # Check if line is an useless line
                contains = False
                for s in contains_list:
                    if s in line:
                        contains = True
                        break
                for s in exact_list:
                    if line.strip().strip('\n') == s:
                        contains = True
                        break
                # Write file without blank lines (if blank_lines=True) and
                # wihtout useless lines
                if blank_lines and line.strip() and not contains \
                        or not blank_lines and not contains:
                    w.write(line)
            w.close()
            # Replace readable_file by cleaned one
            shutil.move(w.name, f)

    def export(self):
        """Exports fields to the desired output"""
        for o in self.objects:
            self.session.add(o)
            try:
                self.session.commit()
            except IntegrityError:
                self.session.rollback()
                pass
    
    # Check if connection has been lost and recovers it.   
    # This method should be removed when sqlalchemy 1.2 is available.
    # Then this methps can be removed and use 
    # engine = create_engine(connection_string, pool_pre_ping=True)
    # (http://docs.sqlalchemy.org/en/latest/core/pooling.html#disconnect-handling-pessimistic)
    @staticmethod
    @event.listens_for(Pool, "checkout")
    def ping_connection(dbapi_connection, connection_record, connection_proxy):
        cursor = dbapi_connection.cursor()
        try:
            cursor.execute("SELECT 1")
        except:
            # raise DisconnectionError - pool will try
            # connecting again up to three times before raising.
            raise exc.DisconnectionError()
        cursor.close()
        
    def  convert_with_pdftotext(self):
        for filename in os.listdir(self.config.raw_data_dir):
            pdftotext('-nopgbrk', '-layout',
                  os.path.join(self.config.raw_data_dir, filename),
                  os.path.join(self.config.data_dir, os.path.splitext(filename)[0] + ".txt"))
        
    def guess_especialitat_code(self, k, cos=None):
        # Look for especialitat code
        # First look for numeric code
        code_match = re.search(r'\b([0-9]{3})\b', k)
        if code_match != None:
            return code_match.group(1)
        # If not found look for letter code or mixed code
        code_match = re.search(r'\b([A-Z]{2,4})\b', k)
        if code_match != None:
            return code_match.group(1)
        code_match = re.search(r'\b([A-Z][0-9]{1,2})\b', k)
        if code_match != None:
            return code_match.group(1)
        k = k.replace('(0,33)', '').replace('(0,50)', '').strip()
        # If code not found, try to find it in especialitats_name table
        k = k.upper()
        esp_desc_table = Table("especialitat_descripcio", 
                               self.metadata, autoload=True)
        
        if cos:
            esp_table = Table("especialitat", self.metadata, autoload=True)
            q = self.session.query(esp_desc_table.c.codi) \
                .join(esp_table) \
                .filter(esp_desc_table.c.descripcio == k) \
                .filter(esp_table.c.cos == cos)
        else:
            q = self.session.query(esp_desc_table.c.codi) \
                .filter(esp_desc_table.c.descripcio == k)
        if q.first():
            code = q.first()[0]
            return code
        # No guess
        return '???'
