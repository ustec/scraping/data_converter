#!/usr/bin/env python
# -*- coding: utf-8 -*-
# converts a pdf into a csv file

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams, LTRect
from pdfminer.converter import PDFPageAggregator
from itertools import islice
import sys, csv
from sh import pdftotext

class CSVWriter:
    """
    A CSV writer which will write rows to CSV file "f"
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        self.writer = csv.writer(f, dialect=dialect, delimiter='$', **kwds)
        self.stream = f

    def writerow(self, row):
        self.writer.writerow([s for s in row])

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


def pdf2csv(pdf, csv_file, left_offset=0, right_offset=0):
    fp = open(pdf, 'rb')
    parser = PDFParser(fp)
    doc = PDFDocument(parser, password='')
    parser.set_document(doc)
    #doc.set_parser(parser)
    # Supply the password for initialization.
    # (If no password is set, give an empty string.)
    #doc.initialize('')
    rsrcmgr = PDFResourceManager()
    # Set parameters for analysis.
    laparams = LAParams()
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    fcsv = open(csv_file, 'w')
    writer = CSVWriter(fcsv)
    for pageno, page in enumerate(PDFPage.create_pages(doc)):
        interpreter.process_page(page)
        layout = device.get_result()
        hlines=[]
        vlines=[]
        for i in layout:
            if not type(i) == LTRect: continue
            hlines.append(int(i.x0))
            hlines.append(int(i.x1))
            vlines.append(int(layout.height - i.y0))
            vlines.append(int(layout.height - i.y1))
        hlines=filterclose(sorted(set(hlines)))
        vlines=filterclose(sorted(set(vlines)))
        i=0
        while(i<len(vlines)-1):
            if not vlines[i+1]-vlines[i]>10:
                i=i+1
                continue
            j=0
            row=[]
            while(j<len(hlines)-1):
                if not hlines[j+1]-hlines[j]>10:
                    j=j+1
                    continue
                row.append(' '.join(get_region(pdf,
                                               pageno+1,
                                               hlines[j]+1,
                                               vlines[i],
                                               hlines[j+1]-1,
                                               vlines[i+1],
                                               left_offset, right_offset).split()))
                j=j+1
            writer.writerow(row)
            i=i+1
    fp.close()
    fcsv.close()

def filterclose(lst):
    tmp=[lst[0]]
    for elem in islice(lst, 1, None):
        if elem - 2 > tmp[-1]:
            tmp.append(elem)
    return tmp

def get_region(pdf, page, x1,y1,x2,y2, left_offset=0, right_offset=0):
    # this is an extremely ugly hack. should be reimplemented with
    # some poppler like lib, which itself only supports getting
    # "selected" text, having some different logic than the
    # simple one used in pdftotext
    return pdftotext('-nopgbrk',
                     '-f', page,
                     '-l', page,
                     '-x', x1 + left_offset,
                     '-y', y1,
                     '-H', abs(y2-y1),
                     '-W', abs(x2-x1) + right_offset,
                     pdf,
                     '-'
                     )

if __name__=='__main__':
    pdf2csv(sys.argv[1], sys.argv[2])
