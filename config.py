#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from abc import ABC
import configparser
import time
import os.path


class Config(ABC):
    """
    This class loads configuration file
    """

    def __init__(self, config_file_path):
        self.CONFIG_FILENAME = config_file_path
        self.config = configparser.ConfigParser()
        self.config.read(self.CONFIG_FILENAME)
        self.raw_data_dir = self.config.get('files', 'RAW_DATA_DIR')
        if self.config.has_option('files', 'DATA_DIR'):
            self.data_dir = self.config.get('files', 'DATA_DIR')
        if self.config.has_option('files', 'URLS'):
            self.urls = self.config.get('files', 'URLS')
        self.log_dir = self.config.get('files', 'LOG_DIR')
        self.log_file = os.path.join(self.log_dir, time.strftime("%Y%m%d.log"))

class ConfigMySql(Config):
    """
    This class loads configuration data related to MySql database
    """

    def __init__(self, config_file_path):
        super(ConfigMySql, self).__init__(config_file_path)
        self.host = self.config.get('database', 'HOST')
        self.user = self.config.get('database', 'USER')
        self.password = self.config.get('database', 'PASSWORD')
        self.database = self.config.get('database', 'DATABASE_NAME')
        self.table = self.config.get('database', 'TABLE_NAME')

    def sqlalchemy_engine_string(self):
        """
        Returns the string connection for SQLAlchemy. 
        Example: 'mysql://user:password@host/database"'
        """
        return "mysql://" + \
               self.user + \
               ":" + \
               self.password + \
               "@" + \
               self.host + \
               "/" + \
               self.database + \
               "?charset=utf8"
