#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import re

def is_date(date):
    ''' Checks if date is in the following format day/month/year. 
        Separator can be / - . '''
    result =  re.fullmatch("[0-9]{1,2}[/\-\.][0-9]{1,2}[/\-\.]([0-9]{2}|[0-9]{4})", date)
    if not result:
        return False
    cd = canonical_date(date)
    try:
        datetime.datetime.strptime(cd, '%d/%m/%Y')
    except ValueError:
        return False
    return True

def canonical_date(date):
    ''' Convert date to canonical date in format dd/mm/yyyy '''
    date = re.sub('\.', '/', date)
    date = re.sub('-', '/', date)
    if re.match("^[0-9][/\-\.]", date):
        date = "0" + date
    if re.match("^[0-9]{2}[/\-\.][0-9][/\-\.]", date):
        date = date[:3] + "0" + date[3:]
    if re.match("^[0-9]{2}[/\-\.][0-9]{2}[/\-\.][0-9]{2}$", date):
        date = date[:6] + "20" + date[6:]
    return date

def first_day_schoolyear():
    ''' Calculates the first day of this school year '''
    today = datetime.date.today()
    if today.month < 9:
        year = today.year - 1
    else:
        year = today.year        
    return datetime.date(year, 9 , 1)
    
def last_day_schoolyear():
    ''' Calculates the last day of this school year '''
    today = datetime.date.today()
    if today.month < 9:
        year = today.year
    else:
        year = today.year + 1
    return datetime.date(year, 8 , 31)